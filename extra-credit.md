# Extra Credit

## EC-1

To neatly type (in LaTex) the lecture notes from one class. If you are
interested, let me know BEFORE the class so that there is at most one scribe /
pair of scribes for each class.  The notes will be due one week after the
lecture.  After receiving the notes, I will provide feedback.  The feedback must
be incorporated before EC points are earned. **This EC will be add 10 points
to your second lowest HW over the course of the semester.**

## EC-2

In March 2019, NCUR 2020 will be hosted by Montana State University.  You are
encouraged to attend this conference (for free!).  The deadline for submitting
abstracts for oral and poster presentations is 6 December 2019. In order to earn
extra credit for submitting to this conference, you must:

* Submit a 1-page proposal via email to the instructor, and request a time to
  meet 1-1.
* Meet 1-1 to disucss your research plan with the instructor (even if under the
  guidance of another mentor).  In this meeting, a research plan will be
  devised, including timing for at least three rounds of revisions of your
  abstract.
* Follow the research plan.
* Your abstract will be graded based on the research plan and feedback provided
  at different stages.  This will count as a homework assignment.

## EC-3

Event Reflection:

* Event: Special Math Seminar
* Speaker: Karen Ulenbeck
* Title: Glimpses into the Calculus of Variations
* Date: 3 September 2019, 16:30-17:30 (refreshments before!)
* Points: 5 points toward H-01.

Throughout this semester, you will have the opportunity to attend various
seminars or events, and write a brief reflection on them for extra credit.
These reflections should be written as a formal two-page paper.  You may
consider the following questions when writing your reflection, but there are no
specific formatting requirements:

* What did you learn?
* Were there any algorithms involved? (Even if not explicitly discussed).
* What is a piece of advise that you took away?
* Did an audience member ask a question that you particularly liked or disliked?
* Is there a question that you wish you could have asked but didn't?
* If related to the event, what were you thinking about as you were leaving?
* Did this event inspire you to look something up after the event? If so, what
  was it?
* Did you meet someone new at this event who you could see as a mentor / mentee
  / collaborator in the future?
* What is your take-away message from this event?

## EC-4

Event Reflection:

* Event: CLS Distinguished Lecture 
* Speaker: Andrew Bernoff
* Title: Why are Raindrops Round?
* Date: 13 September 2019, 14:10-15:00
* Location: Procrastinator
* Points: 5 points toward H-02.

If you can't make that event, there is another event that you can go to as well.
You are welcome to go to both, or to one of them, and to write one reflection.

* Event: CLS Distinguished Lecture 
* Speaker: Deborah G. Johnson
* Title: The Safety of Autonomous Vehicles and the Ethical Responsibilities of Engineers 
* Date:  1 October 2019, 17:30
* Location: Museum of the Rockies
